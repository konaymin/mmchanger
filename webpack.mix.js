const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.styles([
    'public/css/app.css',
    'public/css/vendor/preload.min.css',
    'public/css/vendor/style.light-blue-500.min.css'
], 'public/css/all.css');
mix.scripts([
    'public/js/app.js',
    'public/js/vendor/app.min.js',
    'public/js/vendor/plugins.min.js',
    'public/js/vendor/html5shiv.min.js',
    'public/js/vendor/respond.min.js'
], 'public/js/all.js');