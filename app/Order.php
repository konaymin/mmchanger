<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Order extends Model
{
    public function scopeorderRequested($query, $userID)
    {
        $query->where('status', 'requested')->where('user_id', $userID);
    }

    public function scopeadminCheckOrder($query)
    {
        $query->where('status', 'requested')->orwhere('status', 'processing');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
