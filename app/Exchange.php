<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    public function sendcurrency(){

        return $this->belongsTo('App\Currency','send_currency_id');
    }
    public function receivecurrency(){

        return $this->belongsTo('App\Currency','receive_currency_id');
    }
    public function frule(){

        return $this->belongsTo('App\Rule','forwardrule');
    }
}
