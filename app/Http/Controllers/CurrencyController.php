<?php

namespace App\Http\Controllers;

use Auth;
use App\Currency;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::all();
        return view('currency.index',compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("currency.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = new Currency;
        $currency->type = $request->input('type');
        $currency->desc = $request->input('desc');
        if ($request->hasFile('image')) {
            Image::make(\Input::file('image'))->resize(100, 100)->save();
            $image = $request->file('image');
            $despath = 'uploads/currency/';
            $filename = str_random(6) . '_' . $image->getClientOriginalName();
            $image->move($despath, $filename);
            $currency->img = $despath . $filename;
        }
        $currency->order = $request->input('order');
        $currency->active = $request->input('active');
        $currency->note = $request->input('note');
        $currency->info = $request->input('info');
        $currency->save();
        return redirect('currency');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = Currency::find($id);
        return view('currency.edit',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = Currency::find($id);
        $currency->type = $request->input('type');
        $currency->desc = $request->input('desc');
        $currency->order = $request->input('order');
        $currency->active = $request->input('active');
        $currency->note = $request->input('note');
        $currency->info = $request->input('info');
        $currency->save();
        return redirect('currency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
