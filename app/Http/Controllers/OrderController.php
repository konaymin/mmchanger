<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Currency;
use App\Exchange;
use App\Order;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::where('active', 1)->orderBy('order', 'asc')->pluck('type', 'id');
        return view('order.index', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function exchange(Request $request)
    {
        $send_currency = Currency::find($request->input('sendcurrency'));
        $receive_currency = Currency::find($request->input('receivecurrency'));
        $send_amount = $request->input('amount');
        /**
         * Calculation the exchange
         */
        $exchange = Exchange::where('send_currency_id', $send_currency->id)
            ->where('receive_currency_id', $receive_currency->id)->first();
        if ($exchange->amountlimit < $send_amount) {
            return redirect('/')->with([
                'flash_message' => 'Your order is exceeded the limited amount ' . $exchange->amountlimit . " " . $exchange->sendcurrency->desc,
                'flash_message_important' => true
            ]);
        }
        if($exchange->api == 1)
        {
            $json = file_get_contents('https://api.coinbase.com/v2/exchange-rates?currency='.$send_currency->desc);
            $response = json_decode($json);
            $usd = $response->data->rates->USD;
            $send_amount = $send_amount * $usd;
        }
        $receive_amount = ((( $send_amount * $exchange->frule->amount ) * ($exchange->percentage / 100) ) + $exchange->amount) * $exchange->special;

        return view('order.create', compact('send_currency', 'receive_currency',
            'send_amount', 'receive_amount', 'currencies', 'exchange'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'receive_account' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'attachment' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $order = new Order;
        $order->send_currency = $request->input('send_currency');
        $order->receive_currency = $request->input('receive_currency');
        $order->send_amount = $request->input('send_amount');
        $order->receive_amount = $request->input('receive_amount');
        $order->receive_account = $request->input('receive_account');
        $order->email = $request->input('email');
        $order->phone = $request->input('phone');
        $order->message = $request->input('message');

        if ($request->hasFile('attachment')) {
            Image::make(\Input::file('attachment'))->resize(260, 140)->save();
            $image = $request->file('attachment');
            $despath = 'uploads/attachment/';
            $filename = str_random(6) . '_' . $image->getClientOriginalName();
            $image->move($despath, $filename);
            $order->attachment = $despath . $filename;
        }
        if (Auth::check()) {
            $order->user_id = Auth::id();
        }
        $order->save();
        $currency = Currency::where('type', $order->send_currency)->select('info')->first();
        $exchange = Exchange::where('send_currency',$order->send_currency)
                    ->where('receive_currency',$order->receive_currency)->select('info')->first();
        return view('order.finished',compact('order','currency','exchange'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        if($order->user_id != Auth::id())
        {
            return redirect('/')->with([
                'flash_message' => '403 Forbidden',
                'flash_message_important' => true
            ]);
        }
        return view('order.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if($order->user_id == Auth::id() && $order->status == 'requested'){
            $order->delete();
            return redirect('/orders');
        }
        else
        {
            return redirect('/')->with([
                'flash_message' => '403 Forbidden',
                'flash_message_important' => true
            ]);
        }


    }


    public function history()
    {
        $id = Auth::id();
        if($id)
        {
            $orders = Order::where('user_id',$id)->get();
            return view('order.history', compact('orders'));
        }
        else
        {
            return redirect('/')->with([
                'flash_message' => '403 Forbidden',
                'flash_message_important' => false
            ]);
        }
    }

}
