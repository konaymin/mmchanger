<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Order;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->get();
        return view('admin.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('admin.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'attachment' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $order = Order::find($id);
        $order->status = $request->input('status');
        $order->reply = $request->input('reply');
        if ($request->hasFile('attachment')) {
            Image::make(\Input::file('attachment'))->resize(260, 140)->save();
            $image = $request->file('attachment');
            $despath = 'uploads/reply/';
            $filename = str_random(6) . '_' . $image->getClientOriginalName();
            $image->move($despath, $filename);
            $order->image = $despath . $filename;
        }
        $order->save();
        return redirect('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function check()
    {
        $orders = Order::adminCheckOrder()->get();
        return view('admin.index', compact('orders'));
    }
}
