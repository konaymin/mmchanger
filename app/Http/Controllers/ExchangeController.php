<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Exchange;
use App\Rule;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ExchangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exchanges = Exchange::orderBy('id', 'desc')->get();
        return view('exchange.index',compact('exchanges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::where('active',1)->orderBy('order','asc')->pluck('type','id');
        $rules = Rule::pluck('name','id');
        return view('exchange.create',compact('currencies','rules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $send_currency_id = $request->input('send');
        $receive_currency_id = $request->input('receive');
        $check = Exchange::where('send_currency_id',$send_currency_id)
            ->where('receive_currency_id',$receive_currency_id)->first();
        if($check)
        {
            return redirect('/exchange')->with([
                'flash_message' => 'You have created this exchange rule.Please edit this',
                'flash_message_important' => true
            ]);
        }
        $exchange = new Exchange;
        $send_currency = Currency::find($send_currency_id);
        $receive_currency = Currency::find($receive_currency_id);
        $exchange->send_currency_id = $send_currency->id;
        $exchange->send_currency = $send_currency->type;
        $exchange->receive_currency_id = $receive_currency->id;
        $exchange->receive_currency = $receive_currency->type;

        if($request->input('amount') != null)
        {
            $exchange->amount = $request->input('amount');
        }
        if($request->input('percentage') != null)
        {
            $exchange->percentage = $request->input('percentage');
        }
        if($request->input('special') != null)
        {
            $exchange->special = $request->input('special');
        }
        $exchange->forwardrule = $request->input('forwardrule');
        $exchange->amountlimit = $request->input('amountlimit');
        $exchange->api = $request->input('api');
        $exchange->active = $request->input('active');
        $exchange->info = $request->input('info');
        $exchange->note = $request->input('note');
        $exchange->save();
        return redirect('exchange');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exchange = Exchange::find($id);
        $rules = Rule::pluck('name','id');
        return view('exchange.edit',compact('exchange','rules'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exchange = Exchange::find($id);
        if($exchange->send_currency=="" || $exchange->receive_currency =="")
        {
            $exchange->send_currency = Currency::where('id',$exchange->send_currency_id)->value('type');
            $exchange->receive_currency = Currency::where('id',$exchange->receive_currency_id)->value('type');

        }
        if($request->input('amount') == null)
        {
            $exchange->amount = 0;
        }
        else
        {
            $exchange->amount = $request->input('amount');
        }

        if($request->input('percentage') == null)
        {
            $exchange->percentage = 100;
        }
        else
        {
            $exchange->percentage = $request->input('percentage');
        }

        if($request->input('special') == null)
        {
            $exchange->special = 1;
        }
        else
        {
            $exchange->special = $request->input('special');
        }
        $exchange->forwardrule = $request->input('forwardrule');
        $exchange->amountlimit = $request->input('amountlimit');
        $exchange->api = $request->input('api');
        $exchange->active = $request->input('active');
        $exchange->info = $request->input('info');
        $exchange->note = $request->input('note');
        $exchange->save();
        return redirect('exchange');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function receivecurrency(Request $request)
    {
        $send_currency_id = $request->input('send_currency_id');
//        $receiveCurrency = Exchange::where('send_currency_id',$send_currency_id)->get();
        $receiveCurrency = Exchange::where('send_currency_id',$send_currency_id)->where('active',1)
            ->select('receive_currency_id','receive_currency')->get();
//            ->value('send_currency_id','send_currency');
        return \Response::json($receiveCurrency);
    }
    public function status($id)
    {
        $exchange = Exchange::find($id);
        if($exchange->active == 1)
        {
            $exchange->active = 0;
        }
        else
        {
            $exchange->active = 1;
        }
        $exchange->save();
        return redirect()->back();
    }
}
