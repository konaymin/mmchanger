@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2>Create Currency</h2>
                {!! Form::open(['route' => 'currency.store','files'=>true]) !!}

                @include('currency._form',['submitButtonText'=>'Create Currency'])

                {!! Form::close() !!}

             </div>
        </div>
    <div>
@endsection
