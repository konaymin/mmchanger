@extends('layouts.app')

@section('content')
    <div class="container">

        <a type="button" class="btn btn-primary btn-lg" href="{{ route("currency.create") }}">Create Currency</a>
        <h3>
            Currency table
        </h3>

        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Currency</th>
                <th>Serail_Order</th>
                <th>Status</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($currencies as $currency)
                <tr>
                    <td>{{ $currency->id }}</td>
                    <td>{{ $currency->type}}</td>
                    <td>{{ $currency->order }}</td>
                    <td>{{ $currency->active }}</td>
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ action('CurrencyController@edit', [$currency->id] ) }}">Edit</a>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ action('CurrencyController@destroy', [$currency->id] ) }}">Delete</a>
                        </div>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>

@endsection
