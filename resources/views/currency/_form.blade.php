<div class="form-group">
    {!! Form::label('type','Type') !!}
    {!! Form::text('type',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('desc','Description') !!}
    {!! Form::text('desc',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('order','Serial_Order') !!}
    {!! Form::number('order',null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('status','Status') !!}
    {!! Form::select('active',[1 => 'Enable', 0 => 'Disable'],null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::file('image') !!}
</div>
<div class="form-group">
    {!! Form::label('info','Information') !!}
    {!! Form::textarea('info',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('note','Note') !!}
    {!! Form::text('note',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>
<script>tinymce.init({ selector:'textarea' });</script>