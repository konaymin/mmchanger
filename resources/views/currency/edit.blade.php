@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit: {{ $currency->type }}</h2>
                {!! Form::model($currency,['method'=>'PATCH','route' => ['currency.update',$currency->id]]) !!}

                @include('currency._form',['submitButtonText'=>'Update Currency'])

                {!! Form::close() !!}

            </div>
        </div>
        <div>
@endsection
