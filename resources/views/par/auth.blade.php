<div class="modal-body">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="ms-login-tab">
            <form autocomplete="off" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <fieldset>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
                            <label class="control-label" for="ms-form-user">Email</label>
                            <input type="text" name='email' id="ms-form-user" class="form-control"> </div>
                    </div>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                            <label class="control-label" for="ms-form-pass">Password</label>
                            <input type="password" name='password' id="ms-form-pass" class="form-control"> </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group no-mt">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Remember Me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-raised btn-primary pull-right">Login</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div class="text-center">
                <h3>Login with</h3>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-facebook">
                    <i class="zmdi zmdi-facebook"></i> Facebook</a>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-twitter">
                    <i class="zmdi zmdi-twitter"></i> Twitter</a>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-google">
                    <i class="zmdi zmdi-google"></i> Google</a>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="ms-register-tab">
            <form role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <fieldset>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
                            <label class="control-label" for="ms-form-user">Name</label>
                            <input type="text" id="ms-form-user" class="form-control"
                                   name="name" value="{{ old('name') }}" required autofocus> </div>
                    </div>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                          </span>
                            <label class="control-label" for="ms-form-email">Email</label>
                            <input type="email" id="ms-form-email" class="form-control"
                                   name="email" value="{{ old('email') }}" required> </div>
                    </div>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                            <label class="control-label" for="ms-form-pass">Password</label>
                            <input type="password" id="ms-form-pass" class="form-control"
                                   name="password" required> </div>
                    </div>
                    <div class="form-group label-floating">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                            <label class="control-label" for="ms-form-pass">Re-type Password</label>
                            <input type="password" id="ms-form-pass" class="form-control"
                                   name="password_confirmation" required> </div>
                    </div>
                    <button class="btn btn-raised btn-block btn-primary">Register Now</button>
                </fieldset>
            </form>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="ms-recovery-tab">
            <fieldset>
                <div class="form-group label-floating">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-user">Username</label>
                        <input type="text" id="ms-form-user" class="form-control"> </div>
                </div>
                <div class="form-group label-floating">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-email"></i>
                        </span>
                        <label class="control-label" for="ms-form-email">Email</label>
                        <input type="email" id="ms-form-email" class="form-control"> </div>
                </div>
                <button class="btn btn-raised btn-block btn-primary">Send Password</button>
            </fieldset>
            </form>
        </div>
    </div>
</div>
