<script>
    var rule = parseFloat('{{ $exchange->frule->amount }}');
    var amount = parseFloat('{{ $exchange->amount }}');
    var percentage = parseFloat('{{ $exchange->percentage }}');
    var special = parseFloat('{{ $exchange->special }}');
    var amountlimit = parseFloat('{{ $exchange->amountlimit }}');
    var token = '{{ Session::token() }}';

    $('#send_amount').on('change', function () {
        if ($(this).val() > amountlimit || $(this).val() < 0)
        {
            $(this).val(amountlimit);
            alert('Your Send Amount is over the limit between 0 and '+amountlimit);
        }

        var send_amount = $('#send_amount').val();
        var receive_amount = (((rule*send_amount)*(percentage/100))+amount)*special;
        $('#receive_amount').val(receive_amount.toFixed(3))

    });
</script>