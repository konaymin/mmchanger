<script>

    $('.receive').hide()
    $('#sendcurrency').on('change', function (e) {
        var send_currency_id = e.target.value;
        $.get('/exchange/receive/ajax?send_currency_id=' + send_currency_id, function (data) {
            $('#receivecurrency').empty();
            $.each(data, function (index, Obj) {
                $('#receivecurrency').append('<option value="' + Obj.receive_currency_id + '">' + Obj.receive_currency + '</option>');
                $('.receive').show();
            })

        });
    });
</script>