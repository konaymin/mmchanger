@extends('theme')

@section('content')
    <div class="container">
        <div class="card card-primary card-hero animated fadeInUp animation-delay-7">
            <div class="card-block">
                {!! Form::open(['action' => 'OrderController@exchange','class'=>'form-horizontal','method'=>'get']) !!}
                <fieldset>
                    <div class="form-group">
                        {!! Form::label('send','Send',['class'=>'col-md-2 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::select('sendcurrency',$currencies,null,['class'=>'form-control selectpicker','id'=>'sendcurrency','placeholder' => 'Select the currency...']) !!}
                        </div>
                    </div>

                    <div class="form-group recevie">
                        {!! Form::label('receive','Receive',['class'=>'col-md-2 control-label receive']) !!}
                        <div class="col-md-9">
                            {!! Form::select('receivecurrency',$currencies,null, ['class' => 'form-control receive','id'=>'receivecurrency']) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('amount','Amount',['class'=>'col-md-2 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::number('amount',null,['class'=>'form-control','step'=>'0.0001']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox">
                                    <span class="ml-2">I agree to the terms and conditions.</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            <button class="btn btn-raised btn-primary btn-block mt-4">Exchange</button>
                        </div>
                    </div>
                </fieldset>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- <div class="container">
      <div class="card card-primary card-hero animated fadeInUp animation-delay-7">
        <div class="card-block">
          <form class="form-horizontal">
            <fieldset>
              <div class="form-group">
                <label for="inputUser" class="col-md-2 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="inputUser" placeholder="Username"> </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-md-2 control-label">Email</label>
                <div class="col-md-9">
                  <input type="email" class="form-control" id="inputEmail" placeholder="Email"> </div>
              </div>
              <div class="form-group">
                <label for="inputPassword" class="col-md-2 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" id="inputPassword" placeholder="Password"> </div>
              </div>
              <div class="form-group">
                <label for="inputPassword2" class="col-md-2 control-label">Again</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" id="inputPassword2" placeholder="Password"> </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-md-2 control-label">First Name</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="inputName" placeholder="First Name"> </div>
              </div>
              <div class="form-group">
                <label for="inputLast" class="col-md-2 control-label">Last Name</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="inputLast" placeholder="Last Name"> </div>
              </div>
              <div class="form-group">
                <label for="inputGen" class="col-md-2 control-label">Gender</label>
                <div class="col-md-9">
                  <select id="inputGen" class="form-control selectpicker">
                    <option>Male</option>
                    <option>Female </option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputDate" class="col-md-2 control-label">Bithday</label>
                <div class="col-md-10">
                  <input type="date" class="form-control" id="inputDate" placeholder="Password"> </div>
              </div>
              <div class="row">
                <div class="col-md-offset-2 col-md-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      <span class="ml-2">I agree to the terms and conditions.</span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-md-offset-8">
                  <button class="btn btn-raised btn-primary btn-block mt-4">Register Now</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div> -->
    <!-- container -->
    @include('par.receivecurrency')

@endsection
