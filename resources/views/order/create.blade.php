@extends('theme')

@section('content')
    <div class="container">

        {!! Form::open(['action' => 'OrderController@store','files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('send_currency','Send') !!}
            {!! Form::text('send_currency',$send_currency->type,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('receive_currency','receive') !!}
            {!! Form::text('receive_currency',$receive_currency->type,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('send_amount','Send Amount') !!}
            {!! Form::number('send_amount',$send_amount,['class'=>'form-control','max'=>$exchange->amountlimit]) !!}

        </div>
        <div class="form-group">
            {!! Form::label('receive_amount','Receive Amount') !!}
            {!! Form::number('receive_amount',$receive_amount,['class'=>'form-control','readonly'=>'readonly']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('receive_account',"Enter your ".$receive_currency->type." Account") !!}
            {!! Form::text('receive_account',null,['class'=>'form-control','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email','Contact Email') !!}
            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter Your Email Address','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('phone','Contact Phone') !!}
            {!! Form::text('phone',null,['class'=>'form-control','placeholder'=>'Enter Your Phone Number','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('message','Message') !!}
            {!! Form::textarea('message',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('file','Attachment') !!}
            {!! Form::file('attachment',null,['class'=>'form-control',]) !!}
        </div>
        <div class="row">
            <div class="col-md-4">
                {!! Form::submit('Exchange!',['class'=>'btn btn-primary form-control']) !!}
            </div>
            <div class="col-md-4 col-md-offset-4">
                <a href="/order" id="cancel" name="cancel" class="btn btn-primary form-control">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @include('par.script')
@endsection
