@extends('theme')

@section('content')
    <div class="container">
        <div class="card card-primary card-hero animated fadeInUp animation-delay-7">
            <div class="card-block">
                <h4>Send Currency : {{$order->send_currency}}</h4>
                <h4>Send Amount : {{$order->send_amount}}</h4>
                <h4>Receive Currency : {{$order->receive_currency}}</h4>
                <h4>Receive Amount : {{$order->receive_amount}}</h4>
                <h4>Receive Account : {{$order->receive_account}}</h4>
                <h4>Email : {{$order->email}}</h4>
                <h4>Phone : {{$order->phone}}</h4>

                 <h3>{!! $exchange->info !!}</h3>
                 <h3>{!! $currency->info !!}</h3>
            </div>
        </div>
    </div>

@endsection
