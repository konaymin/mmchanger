@extends('theme')

@section('content')
    <div class="container">
        @if($order->attachment != null)
            <div class="form-group">
                <h3>Attachment</h3>
                <img src="{{asset($order->attachment) }}"/>
            </div>
        @endif
        @if($order->image != null)
            <div class="form-group">
                <h3>Reply</h3>
                <img src="{{asset($order->image) }}"/>
            </div>
        @endif
        <h4>Send Currency : {{ $order->send_currency }}</h4>
        <h4>Send Amount : {{ $order->send_amount }}</h4>
        <h4>Receive Currency : {{ $order->receive_currency }}</h4>
        <h4>Receive Amount : {{ $order->receive_amount }}</h4>
        <h4>Receive Account : {{ $order->receive_account }}</h4>
        <h4>Order Status : {{ $order->status }}</h4>
        <h4>Email : {{ $order->email }}</h4>
        <h4>Phone : {{ $order->phone }}</h4>
        @if($order->message != null)
            <h4>Message : {{ $order->message }}</h4>
        @endif
        @if($order->message != null)
             <h4>Reply : {{ $order->reply }}</h4>
        @endif
            <button type="button" class="btn btn-primary btn-lg btn-block">
                <a href="{{route('orders')}}">Back</a>
            </button>
    </div>
@endsection