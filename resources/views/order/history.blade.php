@extends('theme')

@section('content')
    <div class="container">
        <h3>
            Order History
        </h3>

        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Currency</th>
                <th>Serail_Order</th>
                <th>Status</th>
                <th>Order Time</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->send_currency}}</td>
                    <td>{{ $order->receive_currency }}</td>
                    <td>{{ $order->status }}</td>
                    <td>{{ $order->created_at }}</td>
                    @if($order->status == "requested")
                        <td>
                            <a href="{{route('order/delete/'.$order->id)}}">Delete</a>
                        </td>
                    @else
                        <td>
                            <a href="{{action('OrderController@show',[$order->id])}}">View</a>
                        </td>
                    @endif

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
