@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Special Rule</h2>
                {!! Form::open(['route' => 'rule.store']) !!}


                @include('rule._form',['submitButtonText'=>'Create Special Rule'])

                {!! Form::close() !!}

            </div>
        </div>
        <div>
@endsection
