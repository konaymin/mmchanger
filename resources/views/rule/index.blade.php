@extends('layouts.app')

@section('content')
    <div class="container">

        <a type="button" class="btn btn-primary btn-lg" href="{{ route("rule.create") }}">Create Special Rule</a>
        <h3>
           Sepcial Rule table
        </h3>

        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            @foreach($rules as $rule)
                <tr>
                    <td>{{ $rule->id }}</td>
                    <td>{{ $rule->name }}</td>
                    <td>{{ $rule->amount }}</td>
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ route('rule.edit', [$rule->id] ) }}">Edit</a>
                        </div>
                    </td>


                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection
