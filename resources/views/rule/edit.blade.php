@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Special Rule</h2>
                {!! Form::model($rule,['method'=>'PATCH','route' => ['rule.update',$rule->id]]) !!}

                @include('rule._form',['submitButtonText'=>'Edit Special Rule'])

                {!! Form::close() !!}

            </div>
        </div>
        <div>
@endsection
