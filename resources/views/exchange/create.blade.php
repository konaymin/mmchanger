@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Exchange Rule</h2>
                {!! Form::open(['route' => 'exchange.store']) !!}

                <div class="form-group">
                    {!! Form::label('send','Send') !!}
                    {!! Form::select('send',$currencies,null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('receive','Receive') !!}
                    {!! Form::select('receive',$currencies,null,['class'=>'form-control']) !!}
                </div>

                @include('exchange._form',['submitButtonText'=>'Create Exchange Rule'])

                {!! Form::close() !!}

            </div>
        </div>
        <div>
@endsection
