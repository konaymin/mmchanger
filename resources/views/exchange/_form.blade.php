<div class="form-group">
    {!! Form::label('percentage','Percentage') !!}
    {!! Form::number('percentage',null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('amount','Amount') !!}
    {!! Form::number('amount',null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('special','Special Price') !!}
    {!! Form::number('special',null,['class'=>'form-control','step'=>'0.00001']) !!}

</div>
<div class="form-group">
    {!! Form::label('rule','Special Rule') !!}
    {!! Form::select('forwardrule',$rules,null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('limit','Limited Amount') !!}
    {!! Form::number('amountlimit',null,['class'=>'form-control','step'=>'0.001']) !!}

</div>
<div class="form-group">
    {!! Form::label('api','Use API') !!}
    {!! Form::select('api',[0 => 'Disable', 1 => 'Enable'],null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('status','Status') !!}
    {!! Form::select('active',[1 => 'Enable', 0 => 'Disable'],null,['class'=>'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::label('info','Information') !!}
    {!! Form::textarea('info',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('note','note') !!}
    {!! Form::text('note',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText,['class'=>'btn btn-primary form-control']) !!}
</div>
<script>tinymce.init({ selector:'textarea' });</script>