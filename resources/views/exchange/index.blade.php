@extends('layouts.app')

@section('content')
    <div class="container">

        <a type="button" class="btn btn-primary btn-lg" href="{{ route("exchange.create") }}">Create Exchange Rule</a>
        <h3>
            Exchange table
        </h3>

        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Status</th>
                <th>Send</th>
                <th>Receive</th>
                <th>Amount</th>
                <th>Percentage</th>
                <th>Special</th>
                <th>Limit</th>
                <th>API</th>
                <th>Disable</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            @foreach($exchanges as $exchange)
                <tr>
                    <td>{{ $exchange->id }}</td>
                    <td>{{ $exchange->active }}</td>
                    <td>{{ $exchange->sendcurrency->type }}</td>
                    <td>{{ $exchange->receivecurrency->type }}</td>
                    <td>{{ $exchange->amount }}</td>
                    <td>{{ $exchange->percentage }}</td>
                    <td>{{ $exchange->special }}</td>
                    <td>{{ $exchange->amountlimit }}</td>
                    <td>{{ $exchange->api }}</td>
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ action('ExchangeController@status', [$exchange->id] ) }}">
                                @if($exchange->active==1)
                                    Disable
                                @else
                                    Enable
                                @endif
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ action('ExchangeController@edit', [$exchange->id] ) }}">Edit</a>
                        </div>
                    </td>


                </tr>
            @endforeach
            </tbody>
        </table>
        {{--{{ $exchanges->links() }}--}}
    </div>
    <script>
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>

@endsection
