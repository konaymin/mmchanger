@extends('master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Exchange Rule</h2>
                {!! Form::model($exchange,['method'=>'PATCH','route' => ['exchange.update',$exchange->id]]) !!}

                <div class="form-group">
                    {!! Form::label('send','Send') !!}
                    {!! Form::text('send',$exchange->sendcurrency->type,['class'=>'form-control','readonly'=>'readonly']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('receive','Receive') !!}
                    {!! Form::text('receive',$exchange->receivecurrency->type,['class'=>'form-control','readonly'=>'readonly']) !!}
                </div>

                @include('exchange._form',['submitButtonText'=>'Edit Exchange Rule'])

                {!! Form::close() !!}

            </div>
        </div>
        <div>
@endsection
