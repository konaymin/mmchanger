@extends('layouts.app')

@section('content')
    <div class="container">
        @if($order->attachment != null)
            <div class="form-group">
                <h3>Attachment</h3>
                <img src="{{asset($order->attachment) }}"/>
            </div>
        @endif
        @if($order->image != null)
            <div class="form-group">
                <h3>Reply</h3>
                <img src="{{asset($order->image) }}"/>
            </div>
        @endif
        {!! Form::model($order,['method'=>'PATCH','route'=>['admin.update',$order->id],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('send_currency','Send') !!}
            {!! Form::text('send_currency',null,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('receive_currency','receive') !!}
            {!! Form::text('receive_currency',null,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('send_amount','Send Amount') !!}
            {!! Form::number('send_amount',null,['class'=>'form-control','readonly'=>'readonly']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('receive_amount','Receive Amount') !!}
            {!! Form::number('receive_amount',$order->receive_amount,['class'=>'form-control','readonly'=>'readonly']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('receive_account',"Enter your ".$order->receive_currency." Account") !!}
            {!! Form::text('receive_account',$order->receive_account,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email','Contact Email') !!}
            {!! Form::text('email',$order->email,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('phone','Contact Phone') !!}
            {!! Form::text('phone',$order->phone,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('message','Message') !!}
            {!! Form::textarea('message',$order->message,['class'=>'form-control','readonly'=>'readonly']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('reply','Reply') !!}
            {!! Form::textarea('reply',$order->reply,['class'=>'form-control']) !!}
        </div>
        @if($order->image==null)
                <div class="form-group">
                    {!! Form::label('file','Attachment') !!}
                    {!! Form::file('attachment',null,['class'=>'form-control',]) !!}
                </div>
        @endif

        <div class="row">
            @if($order->status =="requested")
                <div class="col-md-4">
                    <input type="hidden" name="status" id="book_name" class="form-control" value="processing"/>
                    {!! Form::submit('Processing!',['class'=>'btn btn-primary form-control']) !!}
                </div>
            @else
                <div class="col-md-4">
                    <input type="hidden" name="status" id="book_name" class="form-control" value="completed"/>
                    {!! Form::submit('Completed!',['class'=>'btn btn-primary form-control']) !!}
                </div>
            @endif
            <div class="col-md-4 col-md-offset-4">
                <a href="/admin" id="cancel" name="cancel" class="btn btn-primary form-control">Cancel</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
