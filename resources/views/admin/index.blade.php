@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>
            Order table
        </h3>

        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Send</th>
                <th>Receive</th>
                <th>Send Amount</th>
                <th>Receive Amount</th>
                <th>Account</th>
                <th>Status</th>
                <th>CustomerName</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->send_currency }}</td>
                    <td>{{ $order->receive_currency }}</td>
                    <td>{{ $order->send_amount }}</td>
                    <td>{{ $order->receive_amount }}</td>
                    <td>{{ $order->receive_account }}</td>
                    <td>{{ $order->status }}</td>
                    @if(isset($order->user->name))
                        <td>{{ $order->user->name }}</td>
                    @else
                        <td>Guest</td>
                    @endif
                    <td>
                        <div class="form-group">
                            <a type="button" class="btn btn-primary"
                               href="{{ action('AdminController@show', [$order->id] ) }}">Detail</a>
                        </div>
                    </td>


                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>

@endsection
