<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('admin')->resource('currency', 'CurrencyController');
Route::middleware('admin')->resource('exchange', 'ExchangeController');
Route::middleware('admin')->resource('rule', 'RuleController');
Route::middleware('admin')->resource('admin', 'AdminController');
Route::middleware('admin')->get('exchange/{exchange}/status', 'ExchangeController@status');
Route::middleware('admin')->get('check', 'AdminController@check');


Route::get('/', 'OrderController@index');
Route::get('/order/exchange', 'OrderController@exchange');
Route::post('order/store', 'OrderController@store');
Route::get('order/{id}', 'OrderController@show');
Route::get('order/delete/{id}', 'OrderController@destroy');
Route::get('orders', 'OrderController@history')->name('orders');
Route::post('order/exchange', 'OrderController@exchange');
Route::get('/exchange/receive/ajax', 'ExchangeController@receivecurrency');

